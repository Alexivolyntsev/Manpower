﻿namespace LaborCosts.Db.DTO
{
	/// <summary>
	/// Класс для передачи данных записи о трудозатратах.
	/// </summary>
	public class AuditDTO
	{
		/// <summary>
		/// Идентификатор.
		/// </summary>
		public int Id;

		/// <summary>
		/// Наименование проекта.
		/// </summary>
		public string ProjectName;

		/// <summary>
		/// Идентификатор проекта.
		/// </summary>
		public int ProjectId;

		/// <summary>
		/// Затраченное время.
		/// </summary>
		public float Time;

		/// <summary>
		/// Дата записи.
		/// </summary>
		public string Date;

		/// <summary>
		/// Комментарий.
		/// </summary>
		public string Comment;
	}
}