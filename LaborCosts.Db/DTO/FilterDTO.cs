﻿namespace LaborCosts.Db.Models
{
	using Newtonsoft.Json;
	using System;

	/// <summary>
	/// Класс для передачи данных фильтрации грида.
	/// </summary>
	public class FilterDTO
	{
		/// <summary>
		/// Дата.
		/// </summary>
		[JsonProperty("date")]
		public DateTime Date;

		/// <summary>
		/// Наименование проекта.
		/// </summary>
		[JsonProperty("proj.Name")]
		public string ProjectName;

		/// <summary>
		/// Комментарий.
		/// </summary>
		[JsonProperty("Comment")]
		public string Comment;

		/// <summary>
		/// Затраченное время.
		/// </summary>
		[JsonProperty("time")]
		public float Time;
	}
}