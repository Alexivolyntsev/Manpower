﻿namespace LaborCosts.Db
{
	using System;

	/// <summary>
	/// Интерфейс единицы работы с сущностями БД.
	/// </summary>
	public interface IUnitOfWork: IDisposable
	{
		/// <summary>
		/// Открывает транзакцию.
		/// </summary>
		void BeginTransaction();

		/// <summary>
		/// Откатывает транзакцию.
		/// </summary>
		void RollBack();

		/// <summary>
		/// Коммитит транзакцию.
		/// </summary>
		void Commit();
	}
}
