﻿namespace LaborCosts.Db.Models
{
	using Newtonsoft.Json;
	using System;

	/// <summary>
	/// Модель записи о трудозатратах.
	/// </summary>
	public class Audit: BaseIdEntity
	{
		/// <summary>
		/// Затраченное время.
		/// </summary>
		[JsonProperty("time")]
		public virtual float Time { get; set; }

		/// <summary>
		/// Дата выполнения работы.
		/// </summary>
		[JsonProperty("date")]
		public virtual DateTime Date { get; set; }

		/// <summary>
		/// Комментарий.
		/// </summary>
		[JsonProperty("comment")]
		public virtual string Comment { get; set; }

		/// <summary>
		/// Проект.
		/// </summary>
		[JsonProperty("project")]
		public virtual Project Project { get; set; }

	}
}