﻿namespace LaborCosts.Db.Models
{
	using Newtonsoft.Json;

	/// <summary>
	/// Базовый класс сущности БД.
	/// </summary>
	public class BaseIdEntity
	{
		/// <summary>
		/// Идентификатор сущности в БД.
		/// </summary>
		[JsonProperty("id")]
		public virtual int Id { get; set; }
	}
}