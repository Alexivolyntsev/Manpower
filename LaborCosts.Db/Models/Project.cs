﻿namespace LaborCosts.Db.Models
{
	using Newtonsoft.Json;
	using System.Collections.Generic;

	/// <summary>
	/// Модель проекта.
	/// </summary>
	public class Project : BaseIdEntity
	{
		/// <summary>
		/// Имя проекта.
		/// </summary>
		[JsonProperty("name")]
		public virtual string Name { get; set; }

		/// <summary>
		/// Общее количество трудозатрат.
		/// </summary>
		[JsonProperty("totalCost")]
		public virtual float TotalCost { get; set; }

		/// <summary>
		/// Список записей о трудозатратах по проекту.
		/// </summary>
		[JsonProperty("audits")]
		public virtual IList<Audit> Audits {get; set;}
	}
}