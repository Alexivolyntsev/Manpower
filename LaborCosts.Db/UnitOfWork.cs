﻿namespace LaborCosts.Db
{
	using FluentNHibernate.Cfg;
	using FluentNHibernate.Cfg.Db;
	using NHibernate;
	using System;
	using System.IO;
	using System.Reflection;

	/// <summary>
	/// Единица работы с сущностями БД.
	/// </summary>
	public class UnitOfWork : IUnitOfWork
	{

		/// <summary>
		/// Фабрика скссий.
		/// </summary>
		private static readonly ISessionFactory Factory;

		/// <summary>
		/// Транзакция.
		/// </summary>
		private ITransaction _transaction;

		/// <summary>
		/// Сессия.
		/// </summary>
		public ISession Session;

		static UnitOfWork()
		{
			string dbPath;
			var path = $"{Environment.CurrentDirectory}\\connString.txt";
			using (var sr = new StreamReader(path))
			{
				dbPath = sr.ReadToEnd();
			}

			string connectionString = $"Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=\"{dbPath}\";Integrated Security = True";

			Factory = Fluently.Configure()
				.Database(MsSqlConfiguration.MsSql2008.ConnectionString(connectionString))
				.Mappings(x => x.FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly()))
				.BuildSessionFactory();
		}

		public UnitOfWork()
		{
			Session = Factory.OpenSession();
		}

		/// <summary>
		/// Запускает транзакцию.
		/// </summary>
		public void BeginTransaction()
		{
			_transaction = Session.BeginTransaction();
		}

		/// <summary>
		/// Выполняет коммит транзакции.
		/// </summary>
		public void Commit()
		{
			_transaction.Commit();
		}

		/// <summary>
		///  Откат транзакции.
		/// </summary>
		public void RollBack()
		{
			_transaction.Rollback();
		}

		/// <summary>
		/// Завершает единицу работы и выполняет коммитит транзакции.
		/// </summary>
		public void Dispose()
		{
			if (_transaction?.IsActive != true)
				return;

			try
			{
				_transaction.Commit();
			}
			catch (Exception ex)
			{
				_transaction.Rollback();
				Session.Close();
				throw new Exception("Ошибка коммита транзакции.", ex);
			}

			Session.Close();
		}

	}
}