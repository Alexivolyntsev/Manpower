﻿namespace LaborCosts.Db.Maps
{
	using FluentNHibernate.Mapping;
	using Models;

	/// <summary>
	/// Маппинг для сущности проекта.
	/// </summary>
	public class ProjectMap: ClassMap<Project>
	{
		public ProjectMap()
		{
			Table("Projects");
			Id(x => x.Id).GeneratedBy.Identity().Column("id");
			Map(x => x.Name).Column("name");
			Map(x => x.TotalCost).Column("total_cost");
			HasMany(x => x.Audits).KeyColumn("project_id");
		}
	}
}