﻿namespace LaborCosts.Db.Maps
{
	using FluentNHibernate.Mapping;
	using Models;

	/// <summary>
	/// Маппинг для сущности трудозатрат.
	/// </summary>
	public class AuditMap : ClassMap<Audit>
	{
		public AuditMap()
		{
			LazyLoad();
			Table("Audits");
			Id(x => x.Id).GeneratedBy.Identity().Column("id");
			Map(x => x.Date).Column("date");
			Map(x => x.Time).Column("cost");
			Map(x => x.Comment).Column("comment");
			References(x => x.Project);
		}
	}
}