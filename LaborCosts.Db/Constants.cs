﻿namespace LaborCosts.Db
{
	/// <summary>
	/// Константы проекта.
	/// </summary>
	public static class Constants
	{
		/// <summary>
		/// Количество записей на страницу по-умолчанию.
		/// </summary>
		public const int DefaultPageSize = 5;
	}
}