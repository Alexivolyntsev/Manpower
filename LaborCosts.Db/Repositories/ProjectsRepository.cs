﻿namespace LaborCosts.Db.Repositories
{
	using Models;
	public class ProjectsRepository : Repository<Project>
	{
		public ProjectsRepository(UnitOfWork uow) : base(uow)
		{
		}
	}
}