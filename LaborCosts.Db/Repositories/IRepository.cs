﻿namespace LaborCosts.Db.Repositories
{
	using Models;
	using System.Collections.Generic;
	interface IRepository<T> where T: BaseIdEntity
	{

		IList<T> GetAll();

		T Get(int id);

		void Update(T entity);

		void Delete(T entity);

		T Create(T entity);
	}
}