﻿namespace LaborCosts.Db.Repositories
{
	using NHibernate;
	using System;
	using System.Collections.Generic;
	using Models;
	public class Repository<T>: IDisposable, IRepository<T> where T : BaseIdEntity
	{

		private UnitOfWork UoW;

		protected ISession Session { get { return UoW.Session; } }

		protected Repository(IUnitOfWork uow)
		{
			UoW = (UnitOfWork)uow;
		}

		public IList<T> GetAll()
		{
			return Session.QueryOver<T>().List();
		}

		public T Get(int entityId)
		{
			return Session.QueryOver<T>().Where(x => x.Id == entityId ).SingleOrDefault();
		}

		public void Update(T entity)
		{
			Session.Update(entity);
		}

		public void Delete(T entity)
		{
			Session.Delete(entity);
		}

		public T Create(T entity)
		{
			Session.Save(entity);
			return entity;
		}

		public void Dispose()
		{
		}
	}
}