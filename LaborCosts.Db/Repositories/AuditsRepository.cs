﻿namespace LaborCosts.Db.Repositories
{
	using System;
	using Models;
	using NHibernate.Criterion;
	using System.Collections.Generic;
	using DTO;

	/// <summary>
	/// Репозиторий для работы с записями о трудозатратах.
	/// </summary>
	public class AuditsRepository:Repository<Audit>
	{

		/// <summary>
		/// Справочник меодов сортировки.
		/// </summary>
		private readonly Dictionary<string, Func<string, Order>> SortFuncList = new Dictionary<string, Func<string, Order>>
		{
			{ "asc", Order.Asc},
			{ "desc", Order.Desc}
		};

		public AuditsRepository(UnitOfWork uow) : base(uow)
		{
		}

		/// <summary>
		/// Вычисляет количество объектов с учетом фильтра.
		/// </summary>
		/// <param name="filters"> Настройки фильтрации. </param>
		/// <returns> Количество объектов. </returns>
		public int GetFilteredAuditsCount(FilterDTO filters)
		{
			var countCalculatingC = ConstructDetachedFilteredCriteria(filters);
			countCalculatingC.SetProjection(Projections.RowCount());

			var totalCount = (int)countCalculatingC.GetExecutableCriteria(Session).UniqueResult();

			return totalCount;
		}

		/// <summary>
		/// Выполняет получение отфильтрованных записей.
		/// </summary>
		/// <param name="responsedPageNumber"> Запрашиваемый номер страницы пагинатора. </param>
		/// <param name="sortType"> Вид упорядочивания. </param>
		/// <param name="sortFieldName"> Наименования поля для сортировки. </param>
		/// <param name="filters"> Параметры фильтрации. </param>
		/// <returns> Список записей о трудозатратах. </returns>
		public IList<AuditDTO> GetFilteredAuditsList(int responsedPageNumber, string sortType, string sortFieldName, FilterDTO filters)
		{

			var countCalculatingC = ConstructDetachedFilteredCriteria(filters);
			countCalculatingC.SetProjection(Projections.RowCount());

			var totalCount = (int)countCalculatingC.GetExecutableCriteria(Session).UniqueResult();

			var lastPageNumber = (int)Math.Ceiling((double)totalCount / Constants.DefaultPageSize);
			var pageNumber = Math.Min(lastPageNumber, responsedPageNumber);
			var firstResult = (pageNumber - 1) * Constants.DefaultPageSize;

			var resultC = ConstructDetachedFilteredCriteria(filters);
			Func<string, Order> sortFunc;
			SortFuncList.TryGetValue(sortType, out sortFunc);
			resultC.AddOrder(sortFunc(sortFieldName));

			resultC.SetFirstResult(firstResult).SetMaxResults(Constants.DefaultPageSize);

			var criteria = resultC.GetExecutableCriteria(Session);
			var materializedCriteria = criteria.List();

			IList<AuditDTO> audits = new List<AuditDTO>();

			foreach (Audit audit in materializedCriteria)
			{
				audits.Add(new AuditDTO
				{
					ProjectName = audit.Project.Name,
					Date = audit.Date.ToString("dd/MM/yyyy"),
					Comment = audit.Comment,
					Time = audit.Time,
					Id = audit.Id,
					ProjectId = audit.Project.Id
				});
			}

			return audits;
		}

		/// <summary>
		/// Добавляет фильтры по значению к запросу.
		/// </summary>
		/// <param name="dc"> Независимое выражение. </param>
		/// <param name="filters"> Объект фильтров. </param>
		private void ApplyFilters(ref DetachedCriteria dc, FilterDTO filters)
		{
			if (filters == null)
				return;

			if (!string.IsNullOrEmpty(filters.Comment))
				dc.Add(Expression.Like(nameof(filters.Comment), filters.Comment, MatchMode.Anywhere));

			if (!string.IsNullOrEmpty(filters.ProjectName))
				dc.Add(Expression.Like("proj.Name", filters.ProjectName, MatchMode.Anywhere));

			if (filters.Date != default(DateTime))
				dc.Add(Expression.Eq(nameof(filters.Date), filters.Date));

			if(filters.Time != 0F)
				dc.Add(Expression.Eq(nameof(filters.Time), filters.Time));
		}

		/// <summary>
		/// Составляет базовый запрос.
		/// </summary>
		/// <param name="filters"> Фильтры. </param>
		private DetachedCriteria ConstructDetachedFilteredCriteria(FilterDTO filters)
		{
			var dc = DetachedCriteria.For<Audit>();

			dc.CreateAlias("Project", "proj");
			ApplyFilters(ref dc, filters);

			return dc;
		}

	}
}