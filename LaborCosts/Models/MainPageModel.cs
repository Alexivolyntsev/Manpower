﻿namespace LaborCosts.Models
{
	using System.Collections.Generic;
	using Db.DTO;

	/// <summary>
	/// Модель для передачи данных в представление основной страницы.
	/// </summary>
	public class MainPageModel
	{
		/// <summary>
		/// Количество страниц.
		/// </summary>
		public int PageCount { get; set; }

		/// <summary>
		/// Список записей о трудозатратах.
		/// </summary>
		public IEnumerable<AuditDTO> Audits { get; set; }
	}
}