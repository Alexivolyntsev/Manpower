﻿namespace LaborCosts.Controllers
{
	using System;
	using System.Collections.Generic;
	using Microsoft.AspNetCore.Mvc;
	using Db.Repositories;
	using Newtonsoft.Json;
	using Db;
	using Models;
	using Db.DTO;
	using DTO;
	using Db.Models;

	/// <summary>
	/// Контроллер, управляющий представлениями и моделями.
	/// </summary>
	public class ApplicationController : Controller
	{
		/// <summary>
		/// Номер страницы по-умолчанию.
		/// </summary>
		private const int DefaultStartPage = 1;

		/// <summary>
		/// Формирует модель и представление основной страницы.
		/// </summary>
		/// <returns> Представление основной страницы. </returns>
		public IActionResult MainPage()
		{
			var viewModel = new MainPageModel
			{
				Audits = GetAuditsGridData(),
				PageCount = GetGridPageCount()
			};
			return View("MainPage",viewModel);
		}

		/// <summary>
		/// Получает данные для таблицы трудозатрат.
		/// </summary>
		/// <param name="data"> Параметры в формате JSON. </param>
		/// <returns> Представление таблицы и пейджера. </returns>
		[Route("[controller]/[action]")]
		[HttpPost]
		public ActionResult UpdateGridAndPaginator(string data)
		{
			try
			{
				if (data == null)
					throw new ArgumentException("Неверный формат данных для обновления страницы");
				var dataObj = JsonConvert.DeserializeObject<UpdateGridDTO>(data);

				var viewModel = new MainPageModel
				{
					Audits = GetAuditsGridData(dataObj.CurrentPage, dataObj.SortType, dataObj.SortFieldName, dataObj.Filters),
					PageCount = GetGridPageCount(dataObj.Filters)
				};

				return PartialView("PaginatableGrid", viewModel);
			}
			catch (Exception ex)
			{
				//logger.log(ex);
				return new BadRequestObjectResult("Ошибка загрузки данных.");
			}
		}

		/// <summary>
		/// Получает данные для списка проектов.
		/// </summary>
		/// <returns> Представление диалогового окна. </returns>
		[Route("[controller]/[action]")]
		public ActionResult LoadModalView()
		{
			try
			{
				using (var uow = new UnitOfWork())
				using (var projRep = new ProjectsRepository(uow))
				{
					var projects = projRep.GetAll();
					return PartialView("Modal", projects);
				}
			}
			catch (Exception ex)
			{
				//logger.log(ex);
				return new BadRequestObjectResult("Ошибка загрузки списка проектов.");
			}
		}

		public IActionResult Error()
		{
			return View();
		}

		/// <summary>
		/// Получает список записей для таблицы трудозатрат.
		/// </summary>
		/// <param name="currentPage"> Номер текущей страницы таблицы. </param>
		/// <param name="sortType"> Порядок сортировки. </param>
		/// <param name="sortFieldName"> Поле по которому выполняется сортировка. </param>
		/// <param name="filters"> Параметры фильторв. </param>
		/// <returns> Список записей для построения таблицы. </returns>
		private static IEnumerable<AuditDTO> GetAuditsGridData(
			int currentPage = DefaultStartPage,
			string sortType = "asc",
			string sortFieldName = "Date",
			FilterDTO filters = null)
		{
			using (var uow = new UnitOfWork())
			using (var auditsRep = new AuditsRepository(uow))
			{
				var audits = auditsRep.GetFilteredAuditsList(currentPage, sortType, sortFieldName, filters);
				return audits;
			}
		}


		/// <summary>
		/// Вычисляет количество страниц.
		/// </summary>
		/// <param name="filters"> Параметры фильтрации. </param>
		/// <returns> Количество страниц. </returns>
		private static int GetGridPageCount(FilterDTO filters = null)
		{
			using (var uow = new UnitOfWork())
			using (var rep = new AuditsRepository(uow))
			{
				var auditsCount = rep.GetFilteredAuditsCount(filters);
				var pageCount = Convert.ToInt32(Math.Ceiling((double)auditsCount / Constants.DefaultPageSize));
				return pageCount;
			}
		}

	}
}