﻿namespace LaborCosts.Controllers
{
	using System;
	using Microsoft.AspNetCore.Mvc;
	using Db;
	using Db.Repositories;
	using Db.Models;
	using DTO;

	/// <summary>
	/// Контроллер для работы с сущностями аудитов трудозатрат.
	/// </summary>
	[Route("Application/api/[controller]")]
	public class AuditsController : Controller
	{
		/// <summary>
		/// Сохраняет новую запись о трудозатратах.
		/// </summary>
		/// <param name="args"> Параметры записи. </param>
		/// <returns> Http ответ. </returns>
		[HttpPut]
		public IActionResult Put([FromBody]CreateAuditDTO args)
		{
			try
			{
				if (args?.ProjectId == null)
					throw new ArgumentException("Переданы недопустимые значения для сохранения в БД");

				using (var uow = new UnitOfWork())
				using (var auditsRep = new AuditsRepository(uow))
				using (var projectsRep = new ProjectsRepository(uow))
				{
					uow.BeginTransaction();
					var proj = projectsRep.Get(args.ProjectId);

					var audit = new Audit
					{
						Comment = args.Comment,
						Date = args.Date ?? DateTime.Now,
						Time = args.TimeCost,
						Project = proj
					};
					proj.TotalCost += args.TimeCost;
					auditsRep.Create(audit);
					projectsRep.Update(proj);
				}
			}
			catch (Exception ex)
			{
				return new BadRequestObjectResult(ex.Message);
			}
			return new OkResult();
		}

		/// <summary>
		/// Выполняет удаление записи о трудозатратах.
		/// </summary>
		/// <param name="id"> Идентификатор записи. </param>
		[HttpDelete("{id}")]
		public IActionResult Delete(int id)
		{
			try
			{
				using (var uow = new UnitOfWork())
				using (var rep = new AuditsRepository(uow))
				{
					uow.BeginTransaction();
					var audit = rep.Get(id);
					rep.Delete(audit);
				}
				return new OkResult();
			}
			catch (Exception ex)
			{
				return new BadRequestObjectResult(ex.Message);
			}
		}
	}
}
