﻿namespace LaborCosts.DTO
{
	using Db.Models;
	using Newtonsoft.Json;

	/// <summary>
	/// Класс для передачи данных для перезагрузки таблицы.
	/// </summary>
	public class UpdateGridDTO
	{
		/// <summary>
		/// Текущая страница.
		/// </summary>
		[JsonProperty("currentPage")]
		public int CurrentPage;

		/// <summary>
		/// Имя поля по которому выполняется сортировка.
		/// </summary>
		[JsonProperty("sortFieldName")]
		public string SortFieldName;

		/// <summary>
		/// Тип сортировки (убывающая\возрастающая).
		/// </summary>
		[JsonProperty("sortType")]
		public string SortType;

		/// <summary>
		/// Фильтры по значениям.
		/// </summary>
		[JsonProperty("filters")]
		public FilterDTO Filters;
	}
}