﻿namespace LaborCosts.DTO
{
	using Newtonsoft.Json;
	using System;

	/// <summary>
	/// Класс для передачи данных о сохраняемой записи.
	/// </summary>
	public class CreateAuditDTO
	{
		/// <summary>
		/// Идентификатор проекта.
		/// </summary>
		[JsonProperty("projectId")]
		public int ProjectId;

		/// <summary>
		/// Зараченное время.
		/// </summary>
		[JsonProperty("timeCost")]
		public float TimeCost;

		/// <summary>
		/// Дата.
		/// </summary>
		[JsonProperty("date")]
		public DateTime? Date;

		/// <summary>
		/// Комментарий.
		/// </summary>
		[JsonProperty("comment")]
		public string Comment;
	}
}