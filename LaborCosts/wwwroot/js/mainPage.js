﻿var currentPage = 1;
var messageBoxDuration = 4000;
var sortType = "asc";
var fieldName = "Date";

var restAPIUrl = "Application/Api/Audits/";
var loadModalUrl = "Application/LoadModalView/";
var updatePaginatableAreaUrl = "Application/UpdateGridAndPaginator/";

var filters = {};

$(document).ready(function () {

    $("select").material_select();

    $(".datepicker").pickadate({
        selectMonths: true,
        selectYears: 15,
        today: 'Сегодня',
        clear: 'Очистить',
        close: 'Ok',
        closeOnSelect: true
    });

    $(document).on("keydown", ".filter-field", function (e) {
        if (e.keyCode != '13') 
            return;
        saveFilters();
        updatePaginationArea();
    });

    $(document).on("click", ".clear-filter-btn", function (e) {
        filters = {};

        $("#date-filter").val("");
        $("#proj-name-filter").val("");
        $("#time-filter").val("");
        $("#comment-filter").val("");

        updatePaginationArea();
    });

    $(document).on("click", ".filter-btn", function (e) {
        updatePaginationArea();
        saveFilters();
    });

    $(document).on("click", ".sort-control", function () {
        sortType = $(this).data("type");
        fieldName = $(this).data("field");
        updatePaginationArea();
        saveFilters();
    });

    $(document).on("click", ".delete-audit-btn", function (response) {
        var auditId = $(this).data("id");
        $.ajax({
            contentType: "application/json",
            method: "DELETE",
            url: restAPIUrl + auditId,
            success: function (response) {
                updatePaginationArea();
            }
        });
    });
});

function collectFilters() {
    var filtersElements = $(".filter-field");

    var filters = {};
    filtersElements.each(function (index) {
        var $el = $(this);

        var value = $el.val();
        if (!value)
            return;

        var fieldName = $el.data("fieldName");
        filters[fieldName] = value;
    });

    if ($.isEmptyObject(filters))
        return null;
    return filters;
};

function onElementLoad(text, status) {
    if (status === "error") {
        Materialize.toast(text, messageBoxDuration);
    }
    restoreFilters();
};

function collectLoadParams() {
    return {
        data: JSON.stringify({
            CurrentPage: currentPage,
            SortFieldName: fieldName,
            SortType: sortType,
            Filters: collectFilters()
        })
    };
};

function updatePaginationArea() {
    $("#paginatable-grid-holder")
        .load(updatePaginatableAreaUrl,
          collectLoadParams(),
          onElementLoad
    );
};

function addNewAuditNote(projectId, timeCost, date, comment) {
    $.ajax({
        contentType: "application/json",
        method: "PUT",
        url: restAPIUrl,
        data: JSON.stringify({
            projectId: projectId,
            timeCost: timeCost,
            date: date,
            comment: comment
        }),
        success: function (response) {
            updatePaginationArea();
            $("#add-cost-modal").modal('close');
            Materialize.toast('Запись успешно создана.', messageBoxDuration)
        },
        error: function (response) {
            Materialize.toast(response.responseText, messageBoxDuration)
        }
    });
};

function saveFilters() {
    filters = collectFilters();
};

function restoreFilters() {
    if ($.isEmptyObject(filters))
        return;

    $("#date-filter").val(filters["Date"]).select();
    $("#proj-name-filter").val(filters["proj.Name"]).select();
    $("#time-filter").val(filters["Time"]).select();
    $("#comment-filter").val(filters["Comment"]).select().blur();
}